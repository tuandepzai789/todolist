import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RandomDog from '../views/RandomDog.vue'
import DogListName from '../views/DogListName.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomView',
    component: HomeView
  },
  {
    path: '/dog/random',
    name: 'RandomDog',
    component: RandomDog
  },
  {
    path: '/dog',
    name: 'DogListName',
    component: DogListName
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
